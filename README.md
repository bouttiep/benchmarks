# Dépôts pour les benchmarks

En vue de l'acquisition d'un nouveau cluster, ce dépôt rencense différents benchmarks. 

Actuellement, il y a 3 benchmarks applicatifs : 

- Idefix : archive et README dans le dossier Idefix. Inputs contenues dans l'archive.
- SEM46 : Idem dans le dossier SEM46
- CP2K : Idem dans le dossier CP2K. Ce dossier contient les sources et les données standards, un README pour les instructions de construction et d'exécution et 2 fichiers d'inputs. 

Pour chaque benchmark, **les instructions sont contenues dans le fichier README du dossier correspondant**. 


