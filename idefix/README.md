# Benchmark Idefix

Préalable: python3.7+, cmake (>=3.16), un compilateur C++ 17 GNU, OpenMPI.

- Décompresser les sources (lien filesender ci-dessous)
- aller dans le dossier « idefix-bench/benchmark »
- sourcer « sourceMeFirst.sh »
- configurer avec « cmake » (ou en utilisant « ccmake » pour une interface curse qui montre toutes les options activables)
	- Sur CPU en utilisant MPI: cmake $IDEFIX_DIR -DIdefix_MPI=ON
	  Eventuellement rajouter une architecture cible pour avoir les optimisations (par exemple -DKokkos_ARCH_HSW=ON pour Hasewell)

- lancer l’éxécutable avec mpirun
- le temps d’éxécution et les perfs sont données à la fin de idefix.0.log

Optionel: Vérification du résultat (c’est toujours mieux)
- récupérer le fichier de résultat de référence (lien ci-dessous) et le placer dans idefix-bench/benchmark/reference
- lancer le script « checkresult.py » 
- si tout va bien, c’est vert :-)

Le temps d'exdécution est de l'ordre de quelques minutes (Un  peu plus d'une minute sur 128 coeurs). 