# Benchmark SEM46

Ce code écrit en fortran90 avec 2 niveaux de parallélisation MPI, un sur les sources sismiques et un autre sur la décomposition en domaine.

Le package contient notamment:
- un fichier 00README avec comment lancer la compilation et le benchmark.
- un benchmark d’un cas simplifié d’utilisation du code. Il contient la génération d’un maillage, un petit problème direct et inverse.

On a simplifié la procédure: il y a juste 2 make à faire et à lancer le benchmark avec un ./run.sh. Les étapes mentionnées ci-dessus pour le benchmark sont automatisées.

Pour avoir un ordre d’idée, le problème inverse (celui qui est le plus long à tourner) prend autour de 8 min sur 32 coeurs. 
Le benchmark est par défaut lancé sur 32 coeurs mais il peut être lancé sur un nombre de coeurs étant un multiple de 16. Selon les architectures et pour tester en multi-noeuds, il doit être lancé sur 16 * n > nombre de coeurs par noeud (n=nombre de coeurs avec nmin=1 et nmax=24).
